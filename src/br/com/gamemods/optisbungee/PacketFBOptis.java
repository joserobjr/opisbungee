package br.com.gamemods.optisbungee;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import io.netty.buffer.ByteBuf;
import net.md_5.bungee.protocol.packet.DefinedPacket;
import net.md_5.bungee.protocol.packet.PacketFAPluginMessage;


@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
public class PacketFBOptis extends PacketFAPluginMessage
{
	private static Field idField;
	
	public PacketFBOptis(String tag, byte[] data) {
		super(tag, data);
		try {
			if(idField == null)
			{
				Field id = DefinedPacket.class.getDeclaredField("id");
				id.setAccessible(true);
				
				Field modifiersField = Field.class.getDeclaredField("modifiers");
				modifiersField.setAccessible(true);
				modifiersField.setInt(id, id.getModifiers() & ~Modifier.FINAL);
				
				idField = id;
			}
			
			idField.setInt(this, 0xFB);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	public PacketFBOptis() {
		this(null,null);
	}
	
	@Override
	public void writeArray(byte[] b, ByteBuf buf)
    {
        buf.writeInt( b.length );
        buf.writeBytes( b );
    }
	
	@Override
	public byte[] readArray(ByteBuf buf)
    {
        int len = buf.readInt();
        byte[] ret = new byte[ len ];
        buf.readBytes( ret );
        return ret;
    }
}
