package br.com.gamemods.optisbungee;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.protocol.Forge;
import net.md_5.bungee.protocol.Vanilla;

public class OptisBungee extends Plugin {
	@Override
	public void onEnable() {
		try
		{
			Vanilla.getInstance().getClasses()[251] = PacketFBOptis.class;
			Forge.getInstance().getClasses()[251] = PacketFBOptis.class;
		}
		catch(Exception e){e.printStackTrace();}
	}
}
